package com.example.kjankiewicz.android_11w01_mygraphicanimation

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import android.view.WindowManager

class CanonicalSurfaceViewActivity : AppCompatActivity() {
    private lateinit var mOlimpicSurfaceView: MyOlympicSurfaceView

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)

        mOlimpicSurfaceView = MyOlympicSurfaceView(this)
        setContentView(mOlimpicSurfaceView)
    }

    override fun onResume() {
        super.onResume()
        mOlimpicSurfaceView.resume()
    }

    override fun onPause() {
        super.onPause()
        mOlimpicSurfaceView.pause()
    }
}
