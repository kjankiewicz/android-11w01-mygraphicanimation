package com.example.kjankiewicz.android_11w01_mygraphicanimation

import android.content.res.Resources
import android.graphics.Canvas
import android.graphics.Paint
import android.widget.RelativeLayout

internal class MyOlympicCirclesManager(resources: Resources) {

    var ringWidth: Int = 0
    var ringHeight: Int = 0

    var ringLayoutWidth: Int = 0
    var ringLayoutHeight: Int = 0

    var ringColorsArray: IntArray
    var horizontalAlignmentArray: IntArray
    var topMarginArray: IntArray
    var leftMarginArray: IntArray
    var rightMarginArray: IntArray

    private var stage = 0
    private val paint: Paint
    private val strokeWidth = 15


    init {

        val mResources: Resources = resources
        ringWidth = mResources.getDimension(R.dimen.ring_width).toInt()
        ringHeight = mResources.getDimension(R.dimen.ring_height).toInt()

        ringLayoutWidth = mResources.getDimension(R.dimen.ring_layout_width).toInt()
        ringLayoutHeight = mResources.getDimension(R.dimen.ring_layout_height).toInt()

        ringColorsArray = intArrayOf(-0x906801, -0x1000000, -0x10000, -0x9e, -0xd06ee4)
        horizontalAlignmentArray = intArrayOf(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.ALIGN_PARENT_RIGHT)
        topMarginArray = intArrayOf(R.dimen.activity_horizontal_margin, R.dimen.activity_horizontal_margin, R.dimen.activity_horizontal_margin, R.dimen.ring_lower_top_margin, R.dimen.ring_lower_top_margin)
        leftMarginArray = intArrayOf(R.dimen.zero_margin, R.dimen.zero_margin, R.dimen.zero_margin, R.dimen.ring_lower_side_margin, R.dimen.zero_margin)
        rightMarginArray = intArrayOf(R.dimen.zero_margin, R.dimen.zero_margin, R.dimen.zero_margin, R.dimen.zero_margin, R.dimen.ring_lower_side_margin)

        paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.style = Paint.Style.STROKE
        paint.alpha = 255
        paint.strokeWidth = strokeWidth.toFloat()
    }

    fun drawAllOnCanvas(canvas: Canvas, width: Int) {
        val radius = (width - strokeWidth * 5) / 3 / 2
        //int radius = /2 - strokeWidth/2;
        for (i in 0..stage) {
            var realY: Int = radius + strokeWidth / 2
            if (i >= 3)
                realY += radius
            val realX: Int = when (i) {
                0 -> radius + strokeWidth / 2
                1 -> width / 2
                2 -> width - radius - strokeWidth / 2
                3 -> radius * 2 + strokeWidth + strokeWidth / 2
                else -> width - (radius * 2 + strokeWidth + strokeWidth / 2)
            }
            paint.color = ringColorsArray[i]
            canvas.drawCircle(realX.toFloat(), realY.toFloat(),
                    radius.toFloat(), paint)
        }
        stage++
        if (stage == 5) stage = 0
    }
}
