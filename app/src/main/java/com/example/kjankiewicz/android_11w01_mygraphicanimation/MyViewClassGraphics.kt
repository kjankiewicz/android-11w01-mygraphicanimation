package com.example.kjankiewicz.android_11w01_mygraphicanimation

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.Animation
import android.view.animation.AnimationSet
import android.view.animation.AnimationUtils
import android.view.animation.RotateAnimation
import android.view.animation.ScaleAnimation
import android.widget.ImageView
import android.widget.RelativeLayout


class MyViewClassGraphics : AppCompatActivity() {

    internal lateinit var mMOCM: MyOlympicCirclesManager
    internal var anim: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val intent = intent
        val gcw = intent.getStringExtra(MyMainActivity.GRAPHIC_CREATION_WAY)
        anim = intent.getStringExtra(MyMainActivity.ANIMATION_TYPE) == MyMainActivity.ANIMATION_ON

        mMOCM = MyOlympicCirclesManager(resources)

        val imageViewArray = intArrayOf(R.id.blue_circle, R.id.black_circle, R.id.red_circle, R.id.yellow_circle, R.id.green_circle)

        if (gcw == MyMainActivity.STATIC_WAY) {
            setContentView(R.layout.activity_my_view_class_graphics_static)
            if (anim) {
                val piruetAndSaltoAnimation = AnimationUtils.loadAnimation(this, R.anim.piruet_and_salto)
                for (anImageViewArray in imageViewArray) {
                    val circle = findViewById<ImageView>(anImageViewArray)
                    circle.startAnimation(piruetAndSaltoAnimation)
                }
            }
        } else if (gcw == MyMainActivity.PROGRAMMATIC_WAY) {
            setContentView(R.layout.activity_my_view_class_graphics_programmatic)
            // tworzymy te same obiekty w sposób programowy

            val mContent = findViewById<RelativeLayout>(R.id.viewGraphicProgrammaticallyRelativeLayout)

            for (i in 0..4) {

                val mShapeDrawable = GradientDrawable()
                mShapeDrawable.setColor(Color.alpha(Color.WHITE))
                mShapeDrawable.setSize(mMOCM.ringWidth, mMOCM.ringHeight)
                mShapeDrawable.setStroke(20, mMOCM.ringColorsArray[i])
                mShapeDrawable.shape = GradientDrawable.OVAL

                val mImageView = ImageView(applicationContext)
                mImageView.setImageDrawable(mShapeDrawable)
                mImageView.id = imageViewArray[i]

                // Miejsce ImageView wewnątrz RelativeLayout
                val mLayoutParams = RelativeLayout.LayoutParams(
                        mMOCM.ringLayoutHeight, mMOCM.ringLayoutWidth)
                mLayoutParams.addRule(mMOCM.horizontalAlignmentArray[i])
                val topMargin = resources.getDimension(mMOCM.topMarginArray[i]).toInt()
                val leftMargin = resources.getDimension(mMOCM.leftMarginArray[i]).toInt()
                val rightMargin = resources.getDimension(mMOCM.rightMarginArray[i]).toInt()
                mLayoutParams.setMargins(leftMargin, topMargin, rightMargin, 0)

                mImageView.layoutParams = mLayoutParams
                mContent.addView(mImageView)
            }
            if (anim) {
                val rootAnimationSet = AnimationSet(true)
                rootAnimationSet.interpolator = AccelerateDecelerateInterpolator()
                val thinnerScaleAnimation = ScaleAnimation(
                        1f, 0.1f, 1f, 1f,
                        Animation.RELATIVE_TO_SELF, 0.5f,
                        Animation.RELATIVE_TO_SELF, 0.5f)
                thinnerScaleAnimation.duration = 2000
                thinnerScaleAnimation.fillAfter = false
                rootAnimationSet.addAnimation(thinnerScaleAnimation)
                val fatterScaleAnimation = ScaleAnimation(
                        1f, 10f, 1f, 1f,
                        Animation.RELATIVE_TO_SELF, 0.5f,
                        Animation.RELATIVE_TO_SELF, 0.5f)
                fatterScaleAnimation.duration = 2000
                fatterScaleAnimation.startOffset = 2000
                fatterScaleAnimation.fillBefore = false
                rootAnimationSet.addAnimation(fatterScaleAnimation)
                val rotateAnimation = RotateAnimation(0f, 360f,
                        Animation.RELATIVE_TO_SELF, 0.5f,
                        Animation.RELATIVE_TO_SELF, 0.5f)
                rotateAnimation.duration = 2000
                rotateAnimation.startOffset = 2000
                rootAnimationSet.addAnimation(rotateAnimation)

                for (anImageViewArray in imageViewArray) {
                    val circle = findViewById<ImageView>(anImageViewArray)
                    circle.startAnimation(rootAnimationSet)
                }
            }
        } else if (gcw == MyMainActivity.CANVAS_ON_VIEW_WAY) {
            setContentView(R.layout.activity_my_view_class_graphics_programmatic)
            val mContent = findViewById<RelativeLayout>(R.id.viewGraphicProgrammaticallyRelativeLayout)
            val mLayoutParams = RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
            val olympicView = OlympicView(applicationContext)
            olympicView.layoutParams = mLayoutParams
            mContent.addView(olympicView)
            if (anim)
                Thread {
                    while (anim) {
                        olympicView.postInvalidate()
                        try {
                            Thread.sleep(200)
                        } catch (e: InterruptedException) {
                            Log.i(TAG, "InterruptedException")
                        }

                    }
                }.start()
        } else if (gcw == MyMainActivity.CANVAS_ON_SURFACE_VIEW_WAY) {
            setContentView(R.layout.activity_my_view_class_graphics_programmatic)
            val mContent = findViewById<RelativeLayout>(R.id.viewGraphicProgrammaticallyRelativeLayout)
            val mLayoutParams = RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
            val olympicSurfaceView = OlympicSurfaceView(applicationContext)
            olympicSurfaceView.layoutParams = mLayoutParams
            mContent.addView(olympicSurfaceView)
        }
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.my_view_class_graphics, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId
        return id == R.id.action_settings || super.onOptionsItemSelected(item)
    }

    private inner class OlympicView(context: Context) :
            View(context) {
        init {
            if (!anim)
                setOnClickListener { it.invalidate() }
        }

        override fun onDraw(canvas: Canvas) {
            mMOCM.drawAllOnCanvas(canvas, width)
        }
    }

    inner class OlympicSurfaceView(context: Context) :
            SurfaceView(context), SurfaceHolder.Callback {

        private val mSurfaceHolder: SurfaceHolder = holder
        private var mOlympicThread: OlympicThread? = null

        init {
            mSurfaceHolder.addCallback(this)

            if (!anim)
                setOnClickListener {
                    Log.i(TAG, "onClick")
                    if (mOlympicThread != null) {
                        if (!mOlympicThread!!.running) {
                            mOlympicThread!!.setRunning(true)
                            mOlympicThread!!.run()
                        }
                    }
                }
        }

        override fun surfaceCreated(holder: SurfaceHolder) {
            Log.i(OSV_TAG, "surfaceCreated")
        }

        override// Wywoływane także po surfaceCreated
        fun surfaceChanged(holder: SurfaceHolder,
                           format: Int, width: Int, height: Int) {
            Log.i(OSV_TAG, "surfaceChanged")
            if (mOlympicThread == null) {
                mOlympicThread = OlympicThread(holder)
                mOlympicThread!!.setRunning(true)
                mOlympicThread!!.start()
            }
        }

        override fun surfaceDestroyed(holder: SurfaceHolder) {
            Log.i(OSV_TAG, "surfaceDestroyed")
            var retry = true
            mOlympicThread!!.setRunning(false)
            while (retry) {
                try {
                    mOlympicThread!!.join()
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }

                retry = false
            }
        }

        inner class OlympicThread
            internal constructor(private val holder: SurfaceHolder) :
                Thread() {
            var running = false
            private val refreshRate = 200

            override fun run() {
                Log.i(OT_TAG, "run")
                var previousTime: Long
                var currentTime: Long
                previousTime = System.currentTimeMillis()
                var canvas: Canvas? = null
                while (running) {
                    // Look if time has past
                    currentTime = System.currentTimeMillis()

                    while (currentTime - previousTime < refreshRate) {
                        currentTime = System.currentTimeMillis()
                    }
                    previousTime = currentTime

                    try {
                        canvas = holder.lockCanvas()
                        synchronized(holder) {
                            canvas!!.drawColor(Color.WHITE)
                            mMOCM.drawAllOnCanvas(canvas, width)
                        }
                    } finally {
                        if (canvas != null) {
                            holder.unlockCanvasAndPost(canvas)
                        }
                    }
                    try {
                        Thread.sleep((refreshRate - 5).toLong())
                    } catch (e: InterruptedException) {
                        e.printStackTrace()
                    }

                    if (!anim) setRunning(false)
                }
            }

            internal fun setRunning(b: Boolean) {
                Log.i(OT_TAG, "setRunning: $b")
                running = b
            }

        }

    }

    companion object {
        private const val TAG = "MyViewClassGraphics"
        private const val OSV_TAG = "OlympicSurfaceView"
        private const val OT_TAG = "OlympicThread"
    }
}
