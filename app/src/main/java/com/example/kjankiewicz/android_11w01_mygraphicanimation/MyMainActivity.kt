package com.example.kjankiewicz.android_11w01_mygraphicanimation

import android.content.Intent
import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Button

import kotlinx.android.synthetic.main.activity_my_main.*


class MyMainActivity : AppCompatActivity() {

    private lateinit var gallopAnimation: AnimationDrawable

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_main)

        gallopAnimImageView.setBackgroundResource(R.drawable.galop_anim)
        gallopAnimation = gallopAnimImageView.background as AnimationDrawable

        gallopStartButton.setOnClickListener {
            if (!gallopAnimation.isRunning) {
                gallopAnimation.start()
                (it as Button).setText(R.string.stop_gallop)
            } else {
                gallopAnimation.stop()
                (it as Button).setText(R.string.start_gallop)
            }
        }

        viewClassGraphicButton.setOnClickListener {
            val intent = Intent(applicationContext, MyViewClassGraphics::class.java)
            when {
                viewClassGraphicStaticallyRadioButton.isChecked -> intent.putExtra(GRAPHIC_CREATION_WAY, STATIC_WAY)
                viewClassGraphicProgrammaticallyRadioButton.isChecked -> intent.putExtra(GRAPHIC_CREATION_WAY, PROGRAMMATIC_WAY)
                canvasOnViewGraphicRadioButton.isChecked -> intent.putExtra(GRAPHIC_CREATION_WAY, CANVAS_ON_VIEW_WAY)
                canvasOnSurfaceViewGraphicRadioButton.isChecked -> intent.putExtra(GRAPHIC_CREATION_WAY, CANVAS_ON_SURFACE_VIEW_WAY)
                canonicalSurfaceViewGraphicRadioButton.isChecked -> intent.setClass(applicationContext, CanonicalSurfaceViewActivity::class.java)
            }

            if (withAnimationCheckBox.isChecked)
                intent.putExtra(ANIMATION_TYPE, ANIMATION_ON)
            else
                intent.putExtra(ANIMATION_TYPE, ANIMATION_OFF)

            startActivity(intent)
        }

    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.my_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId
        if (id == R.id.action_property_view_animation) {
            val intent = Intent(applicationContext, MyPropertyAnimationActivity::class.java)
            startActivity(intent)
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        var GRAPHIC_CREATION_WAY = "GRAPHIC_CREATION"
        var STATIC_WAY = "STATIC_WAY"
        var PROGRAMMATIC_WAY = "PROGRAMMATIC_WAY"
        var CANVAS_ON_VIEW_WAY = "CANVAS_ON_VIEW_WAY"
        var CANVAS_ON_SURFACE_VIEW_WAY = "CANVAS_ON_SURFACE_VIEW_WAY"
        var ANIMATION_TYPE = "ANIMATION"
        var ANIMATION_ON = "ANIMATION_ON"
        var ANIMATION_OFF = "ANIMATION_OFF"
    }
}
