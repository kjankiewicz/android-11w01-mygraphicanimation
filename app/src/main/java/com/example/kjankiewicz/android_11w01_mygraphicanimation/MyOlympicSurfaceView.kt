package com.example.kjankiewicz.android_11w01_mygraphicanimation


import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.view.SurfaceHolder
import android.view.SurfaceView

class MyOlympicSurfaceView(context: Context) : SurfaceView(context), Runnable {
    private var isRunning = false
    private var loopThread: Thread? = null
    private val mHolder: SurfaceHolder = holder

    private var screenWidth: Int = 0
    private var screenHeight: Int = 0

    private val strokeWidth = 15

    private val circles: Array<Circle>

    init {

        mHolder.addCallback(object : SurfaceHolder.Callback {
            override fun surfaceCreated(holder: SurfaceHolder) {}

            override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {
                screenWidth = width
                screenHeight = height
            }

            override fun surfaceDestroyed(holder: SurfaceHolder) {}
        })

        val radius = 100 //(screenWidth - (strokeWidth * 5)) / 8;
        circles = arrayOf(Circle(150, 150, radius, -0x906801), Circle(550, 350, radius, -0x1000000), Circle(150, 550, radius, -0x10000), Circle(550, 750, radius, -0x9e), Circle(150, 950, radius, -0xd06ee4))
    }

    fun resume() {
        isRunning = true
        loopThread = Thread(this)
        loopThread!!.start()
    }

    fun pause() {
        isRunning = false
        var retry = true
        while (retry) {
            try {
                loopThread!!.join()
                retry = false
            } catch (e: InterruptedException) {
                // try again shutting down the thread
            }

        }
    }

    internal inner class Circle(var x: Int, var y: Int, var radius: Int) {
        var directionX = 1
        var directionY = 1
        var speed = 10
        var color = 0
        val paint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)

        init {
            paint.style = Paint.Style.STROKE
            paint.alpha = 255
            paint.strokeWidth = strokeWidth.toFloat()
        }

        constructor(x: Int, y: Int, radius: Int, color: Int) : this(x, y, radius) {
            this.color = color
            paint.color = color
        }

        fun circlesColliding(subcircle: Circle): Boolean {
            val dx = this.x - subcircle.x
            val dy = this.y - subcircle.y
            val radii = this.radius + subcircle.radius + strokeWidth
            return dx * dx + dy * dy < radii * radii
        }
    }

    private fun updateObjects() {
        var index = 0
        val length = circles.size
        while (index < length) {
            val circle = circles[index]

            if (circle.x - circle.radius - strokeWidth < 0 && circle.directionX < 0 || circle.x + circle.radius + strokeWidth > screenWidth && circle.directionX > 0) {
                circle.directionX *= -1
            }
            if (circle.y - circle.radius - strokeWidth < 0 && circle.directionY < 0 || circle.y + circle.radius + strokeWidth > screenHeight && circle.directionY > 0) {
                circle.directionY *= -1
            }

            for (subindex in index until length) {
                val subcircle = circles[subindex]
                if (circle.circlesColliding(subcircle)) {
                    if (circle.x < subcircle.x && circle.directionX > subcircle.directionX || circle.x > subcircle.x && circle.directionX < subcircle.directionX) {
                        circle.directionX *= -1
                        subcircle.directionX *= -1
                    }
                    if (circle.y < subcircle.y && circle.directionY > subcircle.directionY || circle.y > subcircle.y && circle.directionY < subcircle.directionY) {
                        circle.directionY *= -1
                        subcircle.directionY *= -1
                    }
                }
            }
            circle.x += circle.directionX * circle.speed
            circle.y += circle.directionY * circle.speed
            index++
        }
    }

    private fun renderObjects(canvas: Canvas) {
        canvas.drawColor(Color.WHITE)
        for (circle in circles) {
            canvas.drawCircle(circle.x.toFloat(), circle.y.toFloat(), circle.radius.toFloat(), circle.paint)
        }
    }

    override fun run() {
        while (isRunning) {
            // We need to make sure that the surface is ready
            if (!mHolder.surface.isValid) {
                continue
            }
            val started = System.currentTimeMillis()

            // update - long computation
            updateObjects()
            // draw - short period of locked Canvas
            val canvas = mHolder.lockCanvas()
            if (canvas != null) {
                renderObjects(canvas)
                mHolder.unlockCanvasAndPost(canvas)
            }

            val deltaTime = (System.currentTimeMillis() - started).toFloat()
            var sleepTime = (FRAME_PERIOD - deltaTime).toInt()
            if (sleepTime > 0) {
                try {
                    Thread.sleep(sleepTime.toLong())
                } catch (ignored: InterruptedException) {
                }

            }
            // lost frames - updateObjects without rendering for each lost frame
            while (sleepTime < 0) {
                updateObjects()
                sleepTime += FRAME_PERIOD
            }
        }
    }

    companion object {

        private const val MAX_FPS = 40 //desired fps
        private const val FRAME_PERIOD = 1000 / MAX_FPS // the frame period
    }
}
