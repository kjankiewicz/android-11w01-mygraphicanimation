package com.example.kjankiewicz.android_11w01_mygraphicanimation

import android.animation.AnimatorInflater
import android.animation.AnimatorSet
import android.animation.ArgbEvaluator
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.animation.CycleInterpolator
import android.widget.ImageView

import kotlinx.android.synthetic.main.activity_my_property_animation.*
import android.view.animation.AccelerateInterpolator




class MyPropertyAnimationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_property_animation)

        val imageViewArray = intArrayOf(R.id.blue_circle, R.id.black_circle, R.id.red_circle, R.id.yellow_circle, R.id.green_circle)

        playPropertyAnimationButton.setOnClickListener {

            if (withObjectAnimatorRadioButton.isChecked) {

                for (anImageViewId in imageViewArray) {
                    val circle = findViewById<ImageView>(anImageViewId)
                    val alphaAnim = ObjectAnimator.ofFloat(circle,
                            "alpha", 1f, 0f)
                    alphaAnim.duration = 3000
                    val interpolator = CycleInterpolator(2f)
                    alphaAnim.interpolator = interpolator
                    val backgroundColorAnim = ObjectAnimator.ofObject(circle, "backgroundColor",
                            ArgbEvaluator(), Color.TRANSPARENT, Color.parseColor("#ff0000"),
                            Color.parseColor("#ff7f00"), Color.parseColor("#ffff00"),
                            Color.parseColor("#00ff00"), Color.parseColor("#00ffff"),
                            Color.parseColor("#0000ff"), Color.parseColor("#8b00ff"), Color.TRANSPARENT)
                    backgroundColorAnim.duration = 3000

                    alphaAnim.start()
                    backgroundColorAnim.start()
                }

                val set = AnimatorInflater.loadAnimator(
                        applicationContext,
                        R.animator.property_animator_2_2) as AnimatorSet
                set.setTarget(progressBar)
                set.start()

                /*val progressAnim = ObjectAnimator.ofInt(progressBar, "progress", 0, 100)
                val interpolator = AccelerateInterpolator(2f)
                progressAnim.duration = 3000
                progressAnim.interpolator = interpolator
                val startY = progressBar.y
                val positionUpAnim = ObjectAnimator.ofFloat(progressBar, "y", startY - 100)
                positionUpAnim.duration = 1500

                val positionDownAnim = ObjectAnimator.ofFloat(
                        progressBar, "y", startY - 100, startY)
                positionDownAnim.duration = 1500

                val positionAnim = AnimatorSet()
                positionAnim.play(positionUpAnim).before(positionDownAnim)

                val allProgressBarAnim = AnimatorSet()
                allProgressBarAnim.play(positionAnim).with(progressAnim)

                allProgressBarAnim.start()*/

            } else if (withValueAnimatorRadioButton.isChecked) {

                //final RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.propertyAnimationActivityRelativeLayout);
                for (i in imageViewArray.indices) {
                    val circle = findViewById<ImageView>(imageViewArray[i])

                    val va = ValueAnimator.ofInt(circle.layoutParams.height, 0, circle.layoutParams.height)
                    va.duration = 2000
                    va.startDelay = (i * 500).toLong()

                    va.addUpdateListener { animation ->
                        circle.layoutParams.height = animation.animatedValue as Int
                        circle.requestLayout()
                    }
                    va.start()
                }
            }
        }
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.my_property_animation, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId
        return id == R.id.action_settings || super.onOptionsItemSelected(item)
    }
}
